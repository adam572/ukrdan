#include "Bajt.h"

//!Konwersji liczb i manipulacja na najm�odszym bicie
Bajt::Bajt(int n)
{
	//!Liczba bitow liczby
	 LiczbaBitow=n;
}


Bajt::~Bajt(void)
{
}

//!Binaryzacja atrybutu
void Bajt::Bin(void)
{
	int w, lb=0;
    w=Wartosc;
	do{
        if (w%2 == 0)
            Bity[LiczbaBitow-lb-1]='0';
        else
            Bity[LiczbaBitow-lb-1]='1';
        w/=2;
        lb++;
    }while (lb!=LiczbaBitow);
}

//!Zmiana warto�ci atrybutu na decymaln�
void Bajt::Dec(void)
{
	unsigned long wart = 1;
    Wartosc=0;
	for (int lb=0;lb<LiczbaBitow;lb++)
    {
        if (Bity[LiczbaBitow-lb-1]=='1')
        Wartosc+= wart;
        wart *= 2;
    }
	return;
}


//!Tworzenie 32-bitowej liczby
void Bajt::Zamien(Bajt a, Bajt b, Bajt c, Bajt d)
{
	a.Bin();    b.Bin();    c.Bin();    d.Bin();
    for (int i=0;i<8;i++)   Bity[i]=d.Bity[i];
    for (int i=0;i<8;i++)   Bity[i+8]=c.Bity[i];
    for (int i=0;i<8;i++)   Bity[i+16]=b.Bity[i];
    for (int i=0;i<8;i++)   Bity[i+24]=a.Bity[i];
return;
}

//!Ustawinie warto�� najm�odszego bitu na warto�� 0 lub 1
void Bajt::UstawBit(char i)
{
	Bin();
    Bity[LiczbaBitow-1]=i;
return;
}
