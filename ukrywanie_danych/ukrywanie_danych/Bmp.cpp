#include "Bmp.h"
#include "Bajt.h"

Bmp::Bmp(void)
{
}


Bmp::~Bmp(void)
{

}

//!Wczytanie adresu danych, liczby bit�w kolor�w, oraz szeroko�ci
int Bmp::WczytajPlik(char NazwaPliku[50])
{
	 int Znak;

    Plik=fopen(NazwaPliku,"rb");
    if (Plik==NULL)
        return(0);

//!10B adres obszaru danych (4 bajty)
    fseek(Plik,10,SEEK_SET);
    Bajt *slowo1=new Bajt(8);    Bajt *slowo2=new Bajt(8);    Bajt *slowo3=new Bajt(8);    Bajt *slowo4=new Bajt(8);
    Znak=getc(Plik);    slowo1->Wartosc=Znak;
    Znak=getc(Plik);    slowo2->Wartosc=Znak;
    Znak=getc(Plik);    slowo3->Wartosc=Znak;
    Znak=getc(Plik);    slowo4->Wartosc=Znak;
    Bajt *Dane=new Bajt(32);
    Dane->Zamien(*slowo1,*slowo2,*slowo3,*slowo4);
    Dane->Dec();
	//!Zapis warto�� do atrybutu
    AdresDane=Dane->Wartosc;

//!18B szerokosc
    fseek(Plik,18,SEEK_SET);
    Znak=getc(Plik);    slowo1->Wartosc=Znak;
    Znak=getc(Plik);    slowo2->Wartosc=Znak;
    Znak=getc(Plik);    slowo3->Wartosc=Znak;
    Znak=getc(Plik);    slowo4->Wartosc=Znak;
    Dane->Zamien(*slowo1,*slowo2,*slowo3,*slowo4);
    Dane->Dec();
	//!Zapis warto�� do atrybutu
    Szerokosc=Dane->Wartosc;


//!34B dlugosc obszaru danych (4 bajty)
    fseek(Plik,34,SEEK_SET);
    Znak=getc(Plik);    slowo1->Wartosc=Znak;
    Znak=getc(Plik);    slowo2->Wartosc=Znak;
    Znak=getc(Plik);    slowo3->Wartosc=Znak;
    Znak=getc(Plik);    slowo4->Wartosc=Znak;
	Dane->Zamien(*slowo1,*slowo2,*slowo3,*slowo4);
    Dane->Dec();
	//!Zapis warto�� do atrybutu
    RozmiarDanych=Dane->Wartosc;

delete slowo1;
delete slowo2;
delete slowo3;
delete slowo4;
delete Dane;
return(1);
}

//!Odczytanie z bitmapy tekstu
void Bmp::Wypisz(char PlikTekstowy[10])
{
	int k=0;
    int Znak;
    int Przeczytane=0;
    Tekst=fopen(PlikTekstowy,"wt+");
//!Na ko�cu linii zapisane s� zera nieznacz�ce(0, 1, 2 lub 3, b�d�ce reszt� z dzielenia szeroko�ci przez 4
    int ZeraNiezn = Szerokosc % 4;

//!8 wyzerowanych bit�w na ko�cu tekstu
    int Koniec=0;
    Bajt *bajtZnaku=new Bajt(8);
    Bajt *bajtTMP=new Bajt(8);
    int licznikBitow=0;
    fseek(Plik,AdresDane,SEEK_SET);
    while (k!=RozmiarDanych){
        Znak=getc(Plik);
        Przeczytane++;
        if (Przeczytane<=Szerokosc)
            {
            bajtZnaku->Wartosc=Znak;
            bajtZnaku->Bin();
            bajtTMP->Bity[licznikBitow]=bajtZnaku->Bity[7];
            licznikBitow++;
			//!ci�g 8 wyzerowanych bit�w oznacza koniec ukrytego tekstu
            if (licznikBitow==8)
                {
                licznikBitow=0;
                bajtTMP->Dec();
            if (bajtTMP->Wartosc==0) Koniec=1;
			if (Koniec==0)
                fprintf(Tekst,"%c",bajtTMP->Wartosc);
				}
        }

    if (Przeczytane==Szerokosc+ZeraNiezn)
        Przeczytane=0;
    k++;
}

fclose(Tekst);
fclose(Plik);
delete bajtZnaku;
delete bajtTMP;
return;
}

//!Tworzenie bitmapy z ukrytym tekstem
int Bmp::Kopiuj(char NowaBmp[10], char PlikTekstowy[10])
{
//!bmp z tekstem
    FILE *Plik2;
    int znak;
    int Koniec=0;
    int k=0;
    int Przeczytane=0;
    int BitZnaku=0;
    int ZnakTekstu;
	//!Zera nieznacz�ce
    int ZeraNiezn= Szerokosc % 4;
	Bajt *bajt=new Bajt(8);
    Bajt *bajtZnaku=new Bajt(8);
	//!Otwarcie i odczytanie rozmiaru pliku tekstowego
    Tekst=fopen(PlikTekstowy,"rt+");
    RozmiarTekstu=fseek(Tekst,0L,SEEK_END);
    RozmiarTekstu = ftell(Tekst);
    fseek(Tekst,0L,SEEK_SET);
	//!sprawdzenie czy wystarczy pikseli do ukrycia tekstu z pliku
    if ((RozmiarTekstu+1)*8>RozmiarDanych)
        return(0);
	//!Stowrzenie nowej bitmapy
    Plik2=fopen(NowaBmp,"wb");
	//!Prze�cie do pocz�teku pierwszej bitmapy
    fseek(Plik,0L,SEEK_SET);

//!Przepisanie nag��wka orginalnej bitmapy
    do {
        znak=getc(Plik);
        if (k<AdresDane)
        putc(znak,Plik2);
        k++;
    } while (k<AdresDane);

//!przepisanie danych zmieniaj�c najm�odsze bity kolor�w
    k=0;
    while (k!=RozmiarDanych){
        if ((BitZnaku==0))
            {
            if (Przeczytane<Szerokosc)
                ZnakTekstu=getc(Tekst);
		//!jesli koniec tekstu, wstaw 8 zer (koniec tekstu)
        if (ZnakTekstu==-1)
            {
            bajtZnaku->Wartosc=0;
            bajtZnaku->Bin();
            }
		//!jesli to niekoniec tekstu, wstaw znak
        else{
            bajtZnaku->Wartosc=ZnakTekstu;
            bajtZnaku->Bin();
        }
    }
	//!Pobranie bajtu oryginalnej bitmapy
    znak=getc(Plik);
    Przeczytane++;
//!Gdy nie jest to obszar zer niznacz�cych i nie koniec tekstu	
    if ((Przeczytane<=Szerokosc)&&Koniec==0)
        {
        bajt->Wartosc=znak;
        bajt->Bin();
	//!Zmiana ostatniego bitu na kolejny bit bajtu tekstu
        bajt->UstawBit(bajtZnaku->Bity[BitZnaku]);
        BitZnaku++;
        if (BitZnaku==8)
            {
            BitZnaku=0;
            if (bajtZnaku->Wartosc==0)
            Koniec=1;
            }
        bajt->Dec();
		//!Zapis zmodyfikowanego bajtu do nowej bitmapy
        znak=bajt->Wartosc;
        putc(znak,Plik2);
        } 
//!Gdy juz jest obszar zer niznacz�cych lub koniec tekstu
	else
		putc(znak,Plik2);
        if (Przeczytane==Szerokosc+ZeraNiezn)
            Przeczytane=0;

        k++;
    }

delete bajt;
delete bajtZnaku;
fclose(Plik2);
return 1;
}
